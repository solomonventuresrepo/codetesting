# codetesting

This project consists of a postgres docker image, setup in the docker-compose.yml file, and a RESTful node API for interacting with the database remotely. 

We have been using postman for chrome to test the API.

Starting the project requires running the following command:
```
$> docker-compose up -d
```
