# TestPostGres

This is for running an up-to-date postgres database with pgadmin, the open source web interface for interacting with the database.

## PgAdmin

When this service comes up one needs to use the postgres address for the connection.
This can be found running `getpgip` which is defined by 
``getpgip="docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'"``
in the linux shell scripting home directory definition file.

The address, by default, is the name of the docker image name it is assigned; therefore, upon viewing the `docker-compose.yml` file you will see the name to use.
