var pool = require('./db');

const getHeroes = (req, resp) => {
    pool.query('SELECT * from heroes ORDER BY id ASC', (err, results) => {
        if (err) {
            throw err
        }
        resp.status(200).json(results.rows)
    })
}


module.exports = {
    getHeroes
}