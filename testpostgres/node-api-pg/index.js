const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 3000

/**
 * POST requests will not work if this body parsing language 
 * is not in the code somewhere
 */
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
// End body parser

// I need to figure what this does.
app.use(cors());
// This javascript file defines the API endpoints and connects them to 
// corresponding code.
app.use('/', require('./routes'));

// Begin error handling
// Errors caught in the controllers come back here
app.use(function(req, res, next) {
    var err = new Error('Not found');
    err.status = 404;
    next(err);
});
app.use(function(err, req, res, next) {
    msg = err.message
    if ( err.message.search('duplicate') > -1 ) {
        msg = '[Attempted database duplicate] ' + err.detail
    }
    res.status(err.status || 500);
    res.json({
        message: msg,
        error: {err}
    });
});
// End error handling

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})

