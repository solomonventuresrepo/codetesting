var Heroes = require('../models/Heroes');

/**
 * Purpose: Query db to pull up to 20 heroes from the database and
 *   return them in JSON form by the way of the resp parameter. 
 * General operation:
 *   This variable/function has parameters: request, response, and next.
 * 
 * @param {*} req Contains data from http get/post/put and body data
 * @param {*} resp Object to contain API response to requesting URL
 * @param {*} next Propagates errors to one of the index.js files.
 */
const getHeroes = (req, resp, next) => {
    Heroes.forge()
    .query(function(qb) {
        qb.limit(20);
    })
    .fetchAll()
    .then(function(heroes) {
        resp.status(200).json(heroes);
    })
    .catch(next);
}

/**
 * Purpose: Query db to pull up to 5 heroes from the database based
 *   on the hero moniker and return them in JSON form by the way of 
 *   the resp parameter. 
 * General operation:
 *   This variable/function has parameters: request, response, and next.
 * 
 * @param {*} req Contains data from http get/post/put and body data
 * @param {*} resp Object to contain API response to requesting URL
 * @param {*} next Propagates errors to one of the index.js files.
 */
const getHeroByMoniker = (req, resp, next) => {
   console.log('[getHeroByMoniker] Parameters ' + JSON.stringify(req.params));
   Heroes.forge()
   .query(function (qb) {
       qb.where('moniker', 'LIKE', '%' + req.params['moniker'] + '%');
       qb.limit(5);
   })
   .fetchAll()
   .then(function(heroes) {
       if ( heroes.length > 0 ) {
           resp.status(200).json(heroes);
       } else {
           resp.status(200).json( "No hero with a moniker similar to <" + req.params['moniker'] + ">");
       }
   })
   .catch(next);
}

/**
 * Purpose: Query db to pull up to 5 heroes from the database based
 *   on the hero's govmnt name and return them in JSON form by the 
 *   way of the resp parameter. 
 * General operation:
 *   This variable/function has parameters: request, response, and next.
 * 
 * @param {*} req Contains data from http get/post/put and body data
 * @param {*} resp Object to contain API response to requesting URL
 * @param {*} next Propagates errors to one of the index.js files.
 */
const getHeroByName = (req, resp, next) => {
   console.log('[getHeroByName] Parameters ' + JSON.stringify(req.params));
   Heroes.forge()
   .query(function (qb) {
       qb.where('name', 'LIKE', '%' + req.params['name'] + '%');
       qb.limit(5);
   })
   .fetchAll()
   .then(function(heroes) {
       if ( heroes.length > 0 ) {
           resp.status(200).json(heroes);
       } else {
           resp.status(200).json( "No hero with a name similar to <" + req.params['name'] + ">");
       }
   })
   .catch(next);
}

/**
 * Purpose: Query db to pull up to 5 heroes from the database based
 *   on the hero database id # name and return them in JSON form by the 
 *   way of the resp parameter. 
 * General operation:
 *   This variable/function has parameters: request, response, and next.
 * 
 * @param {*} req Contains data from http get/post/put and body data
 * @param {*} resp Object to contain API response to requesting URL
 * @param {*} next Propagates errors to one of the index.js files.
 */
const getHeroByID = (req, resp, next) => {
   console.log('[getHeroByID] Parameters ' + JSON.stringify(req.params));
   Heroes.forge()
   .query(function (qb) {
       qb.where('id', '=', req.params['id']);
       qb.limit(5);
   })
   .fetchAll()
   .then(function(heroes) {
       if ( heroes.length > 0 ) {
           resp.status(200).json(heroes);
           console.log( "[getHeroByID] " + JSON.stringify(heroes) );
       } else {
           resp.status(200).json( "No hero with the following id <" + req.params['id'] + ">");
       }
   })
   .catch(next);
}

const createHero = (req, resp, next) => {
    console.log('[createHero] with information ' + JSON.stringify(req.body) );
    Heroes.forge({moniker: req.body.moniker, name: req.body.name, description: req.body.description, imgs: req.body.imgs})
    .save()
    .then( function () {
        resp.status(201).json(req.body);
        console.log( '[Create] Added hero ' + req.body.moniker + ' to DB')
    })
    .catch(next);
}

function createUpdateString(req) {
    updateString = "";
    if ( req.body.moniker != null ) {
        updateString += "moniker: '" + req.body.moniker + "'"
    }
    if ( req.body.name != null ) {
        if ( updateString != "" ) { updateString += ", " }
        updateString += "name: '" + req.body.name + "'"
    } 
    if ( req.body.description != null ) {
        if ( updateString != "" ) { updateString += ", " }
        updateString += "description: '" + req.body.description + "'"
    }
    if ( req.body.imgs != null ) {
        if ( updateString != "" ) { updateString += ", " }
        updateString += "imgs: [" + req.body.imgs + "]"
    } 
    result = "{" + updateString + "}";
    return result;
}

const updateHero = ( req, resp, next ) => {
    output = createUpdateString(req);
    var logOutput = "[updateHero] UPDATE \"heroes\" WHERE 'id'=" + req.params.id
                  + " SET " + output;
    console.log(logOutput);
    Heroes.forge({'id': req.params.id}).save({
        moniker: req.body.moniker,
        name: req.body.name,
        description: req.body.description,
        imgs: req.body.imgs
    })
    .then( function(updatedModel) {
        //resp.json(updatedModel.toJSON());
        resp.status(201).json({moniker: req.body.moniker, name: req.body.name, description: req.body.description, imgs: req.body.imgs});
        console.log( '[UpdateHero] updating record with id ' + req.params.id )
    }).catch(next);
    //Heroes.forge({id: req.params.id})
    //.save(output)
}

const deleteHero = (req, resp, next) => {
    console.log('[deleteHero] with value ' + JSON.stringify(req.body) );
    Heroes.forge({id: req.body.id}).fetch()
    .then(function(fetchedModel){
        fetchedModel.destroy();
        resp.status(201).json({id: req.body.id, msg: 'Deleted hero'});
    })
    .catch(next);
}


module.exports = {
    getHeroes,
    getHeroByMoniker,
    getHeroByName,
    getHeroByID,
    deleteHero,
    updateHero,
    createHero
}
