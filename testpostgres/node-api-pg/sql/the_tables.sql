drop table if exists heroes_schema.heroes;
drop table if exists heroes;

create table heroes_schema.heroes (
    id INTEGER primary key,
    moniker TEXT,
    name TEXT,
    description TEXT,
    imgs array(TEXT)
);

insert into heroes (id, moniker, name, description, imgs)
values (
    '11',
    'All Might',
    'Toshinori Yagi',
    'All Might was the eighth holder of the One-For-All Quirk after receiving it from Nana Shimura. He has since passed the torch to Izuku Midoriya, whom he is grooming to be his successor. After using up all the embers of One for All to defeat All For One, All Might retired and ended his era as the world`s greatest hero."',
    ['assets/imgs/allmight.jpg']
);

update heroes_schema.heroes 
set imgs = ['assets/imgs/allmight.jpg','assets/imgs/allmight.png','assets/imgs/Toshinori_manga.png']
where id = 11;

insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '12',
    'Deku',
    'Izuku Midoriya',
    'Deku is the 9th recepient of the quirk: "One-for-All" and has recently unearthed a previously unknown aspect of One-for-All that is similar to All-for-One, in that he can use the individual quirks of the previous possessors of One-for-All.', 
    ['assets/imgs/deku.jpg','assets/imgs/Midoriya_suit.png']
);

insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '17',
    'The Beast Emperor',
    'Matthew Wildcat Darnell',
    'This one is a lion from birth, since 1987 he has been learning the actions and mindsets of the other creations of the world. His empathy, intelligence and strengths make him the natural Emperor amoung his brothers.', 
    ['assets/imgs/beastking.jpg']
);

insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '13',
    'Synch',
    'Everett Thomas',
    'Everett Thomas (Synch) aura`s synchronizes to get powers. Joka born in St. Louis Missouri.',
    ['assets/imgs/synch.jpg']
);

insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '14',
    'Ryder',
    'Ryder',
    'Unintentionally ate Skrull meat, because they had transformed into cows. Some of the meat eaten by people transferred the Skrull`s adaptable DNA code into the human`s cells, resulting in a bizarre condition called Skrullovoria Induced Skrullophobia, in which these individuals not only gained shape-shifting powers equal to, or greater than, actual Skrulls.',
    ['assets/imgs/ryder.jpg']);


insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '16',
    'Prodigy',
    'David Allenye',
    'Normal smart black man, they call him a mutant.',
    ['assets/imgs/prodigy.jpg']
);

insert into heroes_schema.heroes (id, moniker, name, description, imgs)
values (
    '19',
    'Wild Streak',
    'Tamika Bowden',
    'Origin: An exceptional gymnast, Tamika Bowden was under consideration for the Olympics when she lost the use of her legs due to gym equipment sabotage ordered by gangster Big John Buscelli in retaliation for her father Dennis` refusal to join his criminal organization. Dennis, a former Hydra weapon designer, created an exoskeleton to allow her to regain her strength, speed and agility. Dedicating herself to toppling Buscelli`s criminal empire, Tamika, as Wildstreak, accompanied her father in undermining Buscelli`s activities wherever she could find them. One lead brought them to Florida, where she worked alongside the Thing and Psi-Lord against Dreadface, an alien menace allied with Busclli`s rival Manny King. Back in New York, she subsequently worked with demi-god Thunderstrike in exposing one of Busclli`s gun smuggling operations, although their efforts were disrupted by a more murderous criminal opponent, Sangre.  Powers & Abilities:  
While wearing her exoskeleton, Wildstreak possesses enhanced Class 10 strength and speed. As a former professional gymnast, she has been trained as a remarkable acrobat and athlete, skills also augmented by the exoskeleton. 
Paraphernalia:  
Wildstreaks exoskeleton is powered by an internal energy source requiring periodic recharge. She can temporarily increase her physical augmentation at the cost of additional energy expenditure.',
    ['assets/imgs/wildstreak.jpg']
);