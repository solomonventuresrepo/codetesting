var express = require('express');
var router = express.Router();
var controllers = require('./controllers');

router.get('/', controllers.status.get);
router.get('/heroes', controllers.heroes.getHeroes);
router.post('/heroes', controllers.heroes.createHero);
router.get('/hero/moniker/:moniker', controllers.heroes.getHeroByMoniker);
router.get('/hero/name/:name', controllers.heroes.getHeroByName);
router.get('/hero/:id', controllers.heroes.getHeroByID);
router.post('/hero', controllers.heroes.deleteHero);
router.post('/hero/:id', controllers.heroes.updateHero);

module.exports = router;