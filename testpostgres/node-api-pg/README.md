# HEROES RESTful API

This uses Postgres database and NodeJS to create a RESTful API for CRUD operations.

Now using nodemon, which is a nodejs monitoring server that acts like angular and restarts the server when files are modified.

To run the nodejs server app, type the following at the command prompt:

```bash
user@server ~/
> nodemon
```

There are directories for controllers and models. The models refer to tables in the database. The controllers allow for CRUD operations.
Routes.js is a file that expresses where controller data is accessed from the web, the endpoint.

## Models

Tables: heroes

`heroes` schema
| Field | Type| Null | Key | Default |
| --- |  --- | --- | --- | --- |
| id | serial | NO | YES  | NULL |
| moniker | varchar(100) | NO | PRI | NULL |
| name | varchar(100) | NO | PRI  | NULL |
| description | text | YES | NO  | NULL |
| imgs | Test[] | YES | NO | NULL |